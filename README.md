# niota ssl secured node-red with oauth2

## To create new instances

1. Add a IoT Service Builder URL to a niota account the destination you want to run it followed by an instance-name `https://<DESTINATION>/<INSTANCE-NAME>` for example `https://yourdomain.com/node-red-1`. We will proceed woth this example.
2. Save the account and add a corresponding service to the docker-compose file according to the example below
3. Copy the appearing "OAUTH settings" (they appear for Super-Admins only) in the newly added service
4. Adapt the Timezone to your needs

```
  node-red-1:
    image: registry.gitlab.com/digimondo-samples/niota-node-red/docker-niota-node-red:oauth
    volumes:
      - ./data/node-red-1:/data
    restart: always
    user: "0"
    environment:
      - TZ=Europe/Berlin
      - VIRTUAL_HOST=yourdomain.com
      - VIRTUAL_PATH=/node-red-1
      - LETSENCRYPT_HOST=yourdomain.com
      - LETSENCRYPT_EMAIL=support@yourdomain.com
      - OAUTH_CLIENT_ID=xxxxxxxx-477c-4b34-yyyy-2ecdca47bb02
      - OAUTH_CLIENT_SECRET=xxxxxxxx-8ef8-451f-yyyy-65feaebc8ee8
      - OAUTH_AUTH_URL=https://api.niota.io/api/v1/oauth2/authorize
      - OAUTH_TOKEN_URL=https://api.niota.io/api/v1/oauth2/token
      - OAUTH_CALLBACK_URL=https://yourdomain.com/node-red-1/auth/strategy/callback
```
5. Create a folder in data named with your instance-name, e.g. "node-red-1"
6. Copy settings.js there and do not modify it in any way